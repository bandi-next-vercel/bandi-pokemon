import { useState } from "react";

import { GetStaticPaths, GetStaticProps } from "next";
import { NextPage } from "next";

import { Grid, Card, Text, Button, Container, Image } from "@nextui-org/react";

import { Layout } from "../../components/layouts";
import { Pokemon } from "../../interfaces/pokemon-full";
import { localFavorites } from "../../utils";
import confetti from 'canvas-confetti'
import { getPokemonInfo } from '../../utils/getPokemonInfo';

interface Props {
  pokemon: Pokemon;
}

export const PokemonPage: NextPage<Props> = ({ pokemon }) => {
  const { id, name, sprites, dream_world } = pokemon;
  const { front_default, back_default, front_shiny, back_shiny } = sprites;
  const title = `#${id} - ${name}`;

  const [isInFavorites, setIsInFavorites] = useState(
    localFavorites.existInFavorites(id)
  );

  const onToggleFavorite = () => {
    localFavorites.toggleFavorite(id);
    setIsInFavorites( !isInFavorites )

    if( isInFavorites ) return

    confetti({
      zIndex: 999,
      particleCount: 150,
      spread: 160,
      angle: -100,
      origin: {
        x: 1,
        y: 0,
      }
    })
  };

  return (
    <Layout title={title}>
      <Grid.Container css={{ marginTop: "5px" }} gap={2}>
        <Grid xs={12} sm={4}>
          <Card isHoverable css={{ padding: "30px" }}>
            <Card.Body>
              <Card.Image
                src={dream_world || "no-image.png"}
                alt={name}
                width="100%"
                height={200}
              />
            </Card.Body>
          </Card>
        </Grid>

        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header
              css={{ display: "flex", justifyContent: "space-between" }}
            >
              <Text h1 transform="capitalize">
                #{ id } - { name }
              </Text>

              <Button
                color="gradient"
                ghost={ !isInFavorites }
                onPress={ onToggleFavorite }
              >
                { isInFavorites ? 'En Favoritos' : 'Guardar en Favoritos'}
              </Button>
            </Card.Header>

            <Card.Body>
              <Text size={30}>Sprites:</Text>

              <Container direction="row" display="flex" gap={0}>
                <Image
                  src={front_default}
                  alt={name}
                  width={100}
                  height={100}
                />
                <Image src={back_default} alt={name} width={100} height={100} />
                <Image src={front_shiny} alt={name} width={100} height={100} />
                <Image src={back_shiny} alt={name} width={100} height={100} />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  const pokemons451 = [...Array(451)].map((value, index) => `${ index + 1 }`);

  return {
    paths: pokemons451.map(id => ({
      params: {
        id,
      },
    })),
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { id } = params as { id: string };

  const pokemon = await getPokemonInfo( id )

  return {
    props: {
      pokemon,
    },
  };
};

export default PokemonPage;
