import Head from "next/head";
import React, { FC, ReactNode } from "react";
import { Navbar } from "../ui";

interface Props {
  children: ReactNode;
  title?: string;
}

const origin = (typeof window === 'undefined' ? '' : window.location.origin);
export const Layout: FC<Props> = ({ children, title }) => {

  return (
    <>
      <Head>
        <title>{title || "Pokemon App"}</title>
        <meta name="author" content="Albano Fuentes Maya" />
        <meta name="description" content={title} />
        <meta name="keywords" content="****, pokemon, pokedex" />
        <meta
          property="og:title"
          content={`Información sobre ${ title }.`}
        />
        <meta
          property="og:description"
          content={`Esta es la página sobre ${ title }.`}
        />
        <meta
          property="og:image"
          content={ `${origin}/banner.png` }
        />
      </Head>
      <Navbar />

      <main
        style={{
          padding: "10px 20px",
        }}
      >
        {children}
      </main>
    </>
  );
};
