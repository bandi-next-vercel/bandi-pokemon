import { Grid, Card } from "@nextui-org/react";
import { useRouter } from "next/router";
import { NextPage } from 'next';

interface Props {
  id: number;
}

export const FavoriteCardPokemon: NextPage<Props> = ( { id } ) => {

  const router = useRouter();

  const onPress = () => {
    router.push(`/pokemon/${ id }`);
  };

  return (
    <Grid xs={6} sm={3} md={2} xl={1} key={id} onClick={onPress}>
      <Card isHoverable isPressable >
        <Card.Body css={{ p: 1 }}>
          <Card.Image src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${ id }.svg`} width="100%" height={140} />
        </Card.Body>
      </Card>
    </Grid>
  );
};
